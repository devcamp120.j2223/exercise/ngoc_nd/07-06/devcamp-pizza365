// Bước 1 khai báo thư viện mongoose
const mongoose = require('mongoose');
// Bước 2 khai báo thư viện schema
const Schema = mongoose.Schema;
// Bước 3 Khai báo một Schema 
const orderSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true,
    },
    orderCode: {
        type: String,
        unique: true,
    },
    pizzaSize: {
        type: Number,
        required: true,
    },
    pizzaType: {
        type: String,
        required: true,
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drink"
    },
    status: {
        type: String,
        required: true,
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
});
// Bước 4 export Schema
module.exports = mongoose.model("order", orderSchema);