// Bước 1 khai báo thư viện mongoose
const mongoose = require('mongoose');
// Bước 2 khai báo thư viện schema
const Schema = mongoose.Schema;
// Bước 3 Khai báo một Schema 
const drinkSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true,
    },
    maNuocUong: {
        type: String,
        unique: true,
        required: true,
    },
    tenNuocUong: {
        type: String,
        required: true,
    },
    donGia: {
        type: Number,
        required: true,
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
});
// Bước 4 export Schema
module.exports = mongoose.model("drink", drinkSchema);