$(document).ready(function(){
    "use strict";

/*** REGION 1 - Global variables - Vùng khai báo hằng số, tham số TOÀN CỤC */
var gPizzaChangeCombo = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    soLuongNuoc: "",
    thanhTien: ""
};
var gPizzaType = "";

var gUserData = {
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    idVourcher: "",
    loiNhan: "",
};
var gVoucher = {
    giamGia: "",
    phanTramGiamGia: ""
};
var gObjectUserData = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
}
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// Các hàm xử lý sự kiện click nút chọn kích cỡ
$("#btn-size-s").on("click", function(){
    onBtnChangeSize("S")
});
$("#btn-size-m").on("click", function(){
    onBtnChangeSize("M")
});
$("#btn-size-l").on("click", function(){
    onBtnChangeSize("L")
});
// Hàm xử lý các sự kiện chọn Loại Pizza
$("#btn-ocean").on("click", function(){
    onBtnPizzaType("ocean")
});
$("#btn-hawaiian").on("click", function(){
    onBtnPizzaType("hawaiian")
});
$("#btn-cheesy").on("click", function(){
    onBtnPizzaType("cheesy")
});
// Hàm load trang
onPageLoading();
// Hàm xử lý sự kiện hiện modal khi ấn nút gửi
$("#btn-send").on("click", function(){
    onBtnSendClick();
});
// hàm xử lý sự kiện click nút gử đơn hàng
$("#btn-send-modal-don-hang").on("click", function(){
    onBtnSendModalClick();
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm xử lý sự kiện click nút chọn kích cỡ
// Tham số đầu vào là kích cỡ com bo được chọn
function onBtnChangeSize(paramSize){
    "use strict";
    if(paramSize == "S"){
        gPizzaChangeCombo.kichCo = "S";
        gPizzaChangeCombo.duongKinh = 20;
        gPizzaChangeCombo.suon = 2;
        gPizzaChangeCombo.salad = 200;
        gPizzaChangeCombo.soLuongNuoc = 2;
        gPizzaChangeCombo.thanhTien = 150000;
        console.log(gPizzaChangeCombo);
        $("#btn-size-s").removeClass();
        $("#btn-size-s").addClass("btn w-100 btn-success");
        $("#btn-size-m").removeClass();
        $("#btn-size-m").addClass("btn w-100 btn-warning");
        $("#btn-size-l").removeClass();
        $("#btn-size-l").addClass("btn w-100 btn-warning");
    };
    if(paramSize == "M"){
        gPizzaChangeCombo.kichCo = "M";
        gPizzaChangeCombo.duongKinh = 25;
        gPizzaChangeCombo.suon = 4;
        gPizzaChangeCombo.salad = 300;
        gPizzaChangeCombo.soLuongNuoc = 3;
        gPizzaChangeCombo.thanhTien = 200000;
        console.log(gPizzaChangeCombo);
        $("#btn-size-s").removeClass();
        $("#btn-size-s").addClass("btn w-100 btn-warning");
        $("#btn-size-m").removeClass();
        $("#btn-size-m").addClass("btn w-100 btn-success");
        $("#btn-size-l").removeClass();
        $("#btn-size-l").addClass("btn w-100 btn-warning");
    }
    if(paramSize == "L"){
        gPizzaChangeCombo.kichCo = "L";
        gPizzaChangeCombo.duongKinh = 30;
        gPizzaChangeCombo.suon = 8;
        gPizzaChangeCombo.salad = 500;
        gPizzaChangeCombo.soLuongNuoc = 4;
        gPizzaChangeCombo.thanhTien = 250000;
        console.log(gPizzaChangeCombo);
        $("#btn-size-s").removeClass();
        $("#btn-size-s").addClass("btn w-100 btn-warning");
        $("#btn-size-m").removeClass();
        $("#btn-size-m").addClass("btn w-100 btn-warning");
        $("#btn-size-l").removeClass();
        $("#btn-size-l").addClass("btn w-100 btn-success");
    }
};

// Hàm xử lý sự kiện lựa chọn Loại Pizza 
// Tham số đầu vào là loại pizza được chọn 
function onBtnPizzaType(paramPizzaType){
    "use strict";
    if (paramPizzaType == "ocean"){
        gPizzaType = "ocean";
        console.log(gPizzaType);
        $("#btn-ocean").removeClass();
        $("#btn-ocean").addClass("btn btn-success w-100");
        $("#btn-hawaiian").removeClass();
        $("#btn-hawaiian").addClass("btn btn-warning w-100");
        $("#btn-cheesy").removeClass();
        $("#btn-cheesy").addClass("btn btn-warning w-100");
    };
    if (paramPizzaType == "hawaiian"){
        gPizzaType = "hawaiian";
        console.log(gPizzaType);
        $("#btn-ocean").removeClass();
        $("#btn-ocean").addClass("btn btn-warning w-100");
        $("#btn-hawaiian").removeClass();
        $("#btn-hawaiian").addClass("btn btn-success w-100");
        $("#btn-cheesy").removeClass();
        $("#btn-cheesy").addClass("btn btn-warning w-100");
    };
    if (paramPizzaType == "cheesy"){
        gPizzaType = "cheesy";
        console.log(gPizzaType);
        $("#btn-ocean").removeClass();
        $("#btn-ocean").addClass("btn btn-warning w-100");
        $("#btn-hawaiian").removeClass();
        $("#btn-hawaiian").addClass("btn btn-warning w-100");
        $("#btn-cheesy").removeClass();
        $("#btn-cheesy").addClass("btn btn-success w-100");
    };
};
function onPageLoading(){
    "use strict";
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
        type: "GET",
        dataType: "json",
        success: function(response){
            //console.log(response);
            getDrinkSelect(response);
        },
        error: function(){
            alert("Gọi Api không thành công!")
        }
    })
}
// Hàm xử lý sự kiện khi ấn nút gửi để hiện modal
function onBtnSendClick(){
    "use strict";
    // Bước 0: tạo biến chứ dữ liệu người dùng
    
    // Bước 1: lấy thông tin người dùng
    getImputDataUser(gUserData);
    //console.log(vUserData);
    // Bước 2: Kiểm tra dữ liệu đầu vào 
    var vCheck = getCheckDataUser(gUserData);
    // Bước 3: Hiển thị ra modal
    if ( vCheck == true){
        // Gọi Api lấy mã giảm giá
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + gUserData.idVourcher,
            type: "GET",
            dataType: "json",
            success: function(response){
                gVoucher.phanTramGiamGia = response.phanTramGiamGia;
                getDisplayModal(gUserData);
                $("#modal-thong-tin-khach-hang").modal("show");  
            },
            error: function(){
                alert("Mã giảm giá không đúng!");
                gVoucher.phanTramGiamGia = 0;
                getDisplayModal(gUserData);
                $("#modal-thong-tin-khach-hang").modal("show");
            }
        });
    
    }
    
};
// Hàm xử lý sự kiện gửi đơn hàng 
function onBtnSendModalClick(){
    "use strict";
    var vIdVoucher = $("#select-drink").val();
    gObjectUserData.kichCo = gPizzaChangeCombo.kichCo;
    gObjectUserData.duongKinh = gPizzaChangeCombo.duongKinh;
    gObjectUserData.suon = gPizzaChangeCombo.suon;
    gObjectUserData.salad = gPizzaChangeCombo.salad;
    gObjectUserData.loaiPizza = gPizzaType;
    gObjectUserData.idVourcher = gUserData.idVourcher;
    gObjectUserData.idLoaiNuocUong = vIdVoucher;
    gObjectUserData.soLuongNuoc = gPizzaChangeCombo.soLuongNuoc;
    gObjectUserData.hoTen = gUserData.hoTen;
    gObjectUserData.thanhTien = gPizzaChangeCombo.thanhTien;
    gObjectUserData.email = gUserData.email;
    gObjectUserData.soDienThoai = gUserData.soDienThoai;
    gObjectUserData.diaChi = gUserData.diaChi;
    gObjectUserData.loiNhan = gUserData.loiNhan;
    console.log(gObjectUserData);
    $("#modal-thong-tin-khach-hang").modal("hide");
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    $.ajax({
        url: vBASE_URL,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(gObjectUserData),
        success: function(response){
            $("#inp-user-id").val(response.orderId);
            $("#modal-result").modal("show");
        }
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm đổ dữ liệu vào Select Chọn đồ uống
// Tham số đầu vào là dữ liệu được gọi từ Api về
function getDrinkSelect(paramResponse){
    "use strict";
    var vSelectDrink = $("#select-drink");
    for (var bI = 0; bI < paramResponse.length; bI++){
        vSelectDrink.append($("<option>", {
            text: paramResponse[bI].tenNuocUong,
            value: paramResponse[bI].maNuocUong
        }));
    }
};
// Hàm lấy thông tin khách hàng
// giá trị đầu vào là biến chứ thông tin khách hàng
function getImputDataUser(paramUserData){
    "use strict";
    paramUserData.hoTen = $("#inp-ho-ten").val();
    paramUserData.email = $("#inp-email").val();
    paramUserData.soDienThoai = $("#inp-so-dien-thoai").val();
    paramUserData.diaChi = $("#inp-dia-chi").val();
    paramUserData.idVourcher = $("#inp-ma-giam-gia").val();
    paramUserData.loiNhan = $("#inp-loi-nhan").val();
};
// Hàm kiểm tra dữ liệu
function getCheckDataUser(paramUserData){
    "use strict";
    var vFound = true;
    for (let bI in paramUserData){
        if (bI != "idVourcher" && bI != "loiNhan"){
            if (paramUserData[bI] == ""){
                alert("Hãy nhập thông tin vào ô " + bI);
                vFound = false;
            }
        }
    };
    if (gPizzaType == ""){
        alert("Hãy chọn loại Pizza của bạn!");
        vFound = false;
    };
    if (gPizzaChangeCombo.kichCo == ""){
        alert("Hãy chọn combo Pizza của bạn!");
        vFound = false;
    };
    if ($("#select-drink").val() == 0){
        alert("Hãy chọn đồ uống phù hợp với bạn!");
        vFound = false;
    };
    return vFound;
};
// Hàm hiển thị ra modal
function getDisplayModal(paramData){
    "use strict";
    gVoucher.giamGia = gPizzaChangeCombo.thanhTien * gVoucher.phanTramGiamGia / 100;
    var vThanhToan = gPizzaChangeCombo.thanhTien - gVoucher.giamGia;
    $("#inp-modal-ho-ten").val(paramData.hoTen);
    $("#inp-modal-so-dien-thoai").val(paramData.soDienThoai);
    $("#inp-modal-dia-chi").val(paramData.diaChi);
    $("#inp-modal-loi-nhan").val(paramData.loiNhan);
    $("#inp-modal-giam-gia").val(paramData.idVourcher);
    var vData = "Xác Nhận: " ;
    vData += paramData.hoTen + ", " + paramData.soDienThoai + ", " + paramData.diaChi;
    vData += ", Menu: " + gPizzaChangeCombo.kichCo + ", Sườn Nướng: " + gPizzaChangeCombo.suon + ", Nước: " + gPizzaChangeCombo.soLuongNuoc + ", Salad " + gPizzaChangeCombo.salad ;
    vData += ", Loại Pizza: " + gPizzaType + ", Giá: " + gPizzaChangeCombo.thanhTien + ", Mã giảm giá " + paramData.idVourcher;
    vData += ", Phải thanh toán: " + vThanhToan;
    $("#textarea-modal-detail").html(vData);

}
})