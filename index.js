const express = require("express");
//Khai báo thư viện path
const path = require('path');
const app = express();
const port = 8000;

// Khai báo Schema
const drinkModel = require('./app/model/drinkModel');
const voucherModel = require('./app/model/voucherModel');
const orderModel = require('./app/model/orderModel');

// Khai báo thư viện mongoose
const mongoose = require('mongoose');
const { error } = require("console");

// kết nối với mongoose
mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (error) => {
    if (error){
        throw error;
    };
    console.log('Successfully connected')
})
// Khai báo thư viện static
app.use(express.static("views"));

// get Pizza365
app.get('/', (request, response) => {
    console.log(__dirname)
    response.sendFile(path.join(__dirname + "/views/Pizza365.html"))
});

// get danh sach don hang
app.get('/danh_sach_don_hang.html', (request, response) => {
    console.log(__dirname)
    response.sendFile(path.join(__dirname + "/views/danh_sach_don_hang.html"))
});

app.listen(port, (request, response) => {
    console.log(`Đã vào cổng localhost ${port}`)
})